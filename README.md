Aries NPC Generator


Feel free to modify/fork/create a pull request.


In order to modify the races/classes go to the models folder and hack away at either the classes list json or the races list json.


The application was written in Netbeans (which means it was serving the files as if it were a server) if you wish to modify the files and read them directly from your hard drive I recommend using Firefox as your browser.


This software is released under the MIT License.
