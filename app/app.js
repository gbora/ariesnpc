angular.module('npcGen', ['ngChance', 'ngNPC', 'ui.bootstrap', 'angularSpinner', 'ngLodash', 'xeditable'])

        .run(function (editableOptions) {
            editableOptions.theme = 'bs3';
        })

        .controller('mainCtrl', function (npcFactory, $scope, usSpinnerService, lodash) {

            $scope.downloadLink = null;
            $scope.aboutMode = false;

            $scope.toggleAboutMode = function () {
                $scope.aboutMode = !$scope.aboutMode;
            };

            $scope.getSocialRankDescription = function (code) {
                var descriptions = ['peasant', 'poor city dweller or workman',
                    'afluent city dweller', 'lesser noble', 'great noble'];
                
                 var socDescription = 'Simmilar to a ';
                 if (code <= descriptions.length) {
                     return socDescription + descriptions[code] + '.';
                 } else {
                     return socDescription + descriptions[(descriptions.length -1)] + '.';
                 }
            };

            $scope.startSpin = function () {
                usSpinnerService.spin('spinner-1');
            };

            $scope.stopSpin = function () {
                usSpinnerService.stop('spinner-1');
            };

            $scope.generateDownloadCSV = function () {
                var csvContent = "data:text/txt;charset=utf-8,";
                csvContent += $scope.npc.firstName + " " + $scope.npc.lastName + ",\n";
                csvContent += $scope.npc.title + "\n";
                csvContent += "Mother " + $scope.npc.mother.firstName + " " + $scope.npc.mother.lastName + "\n";
                csvContent += "Father " + $scope.npc.father.firstName + " " + $scope.npc.father.lastName + "\n";

                var encodedUri = encodeURI(csvContent);
                window.open(encodedUri);
            };


            $scope.initNpcList = function () {
                $scope.startSpin();

                $scope.npcList = [];
                $scope.npc = {};
                $scope.npcIndex = 0;


                npcFactory.getFullNpc('The First').then(function (data) {
                    $scope.npcList.push(data);
                    $scope.npc = $scope.npcList[0];
                    $scope.npcIndex = 0;
                    $scope.stopSpin();
                });


            };

            $scope.checkIfNpcExists = function (npc) {
                var npcInList = false;

                //If the npc is in the list npcInList will be a truthy object but 
                //if npc doesn't exists it will be undefined a falsey object
                npcInList = lodash.find($scope.npcList, {'firstName': npc.firstName,
                    'lastName': npc.lastName});


                if (!npcInList) {
                    return false;
                } else {
                    return true;
                }


            };

            $scope.addRelative = function (relative, marriedIn) {
                $scope.startSpin();
                var tempRelative = npcFactory.fillRelative(relative, marriedIn);
                if (!$scope.checkIfNpcExists(tempRelative)) {
                    $scope.npcList.push(tempRelative);
                }
                $scope.stopSpin();
            };

            $scope.addSibling = function (relative, index) {
                $scope.startSpin();
                var tempSibling = npcFactory.fillRelative(relative);
                tempSibling.father = $scope.npc.father;
                tempSibling.mother = $scope.npc.mother;

                var tempSiblingList = angular.copy($scope.npc.siblings);
                tempSiblingList[index] = $scope.npc;

                tempSibling.siblings = tempSiblingList;
                if (!$scope.checkIfNpcExists(tempSibling)) {
                    $scope.npcList.push(tempSibling);
                }
                $scope.stopSpin();
            };

            $scope.switchFocus = function (index) {
                $scope.npc = $scope.npcList[index];
                $scope.npcIndex = index;
            };

            $scope.initNpcList();

        });