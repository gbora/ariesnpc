angular.module('ngClassServer', ['ngChance'])
        .factory('classFactory', function (chance) {

            var _checkClass = function (charClass, attributes) {

                var qualifies = 0;

                for (var i = 0; i < charClass.statRequirements.length; i++) {
                    var requirement = charClass.statRequirements[i];

                    if (attributes[requirement.atrributeId].attributeValue >= requirement.attributeMinimum) {
                        qualifies++;
                    }
                }


                if (qualifies >= 2) {
                    return true;
                } else {
                    return false;
                }
            };

            var _getBestStat = function (attributes) {
                var maxStatId = attributes[0].atrributeId;
                var maxStatValue = attributes[0].attributeValue;

                for (var i = 0; i < attributes.length; i++) {
                    if (attributes[i].attributeValue > maxStatValue) {
                        maxStatId = attributes[i].atrributeId;
                        maxStatValue = attributes[i].attributeValue;
                    }
                }
                
                return maxStatValue;
            };

            var _getClass = function (attributes, classes) {
                //console.log('here are the classes',classes.length);
                //console.log('here are the attributes', attributes);

                var qualifiedClasses = [];
                var favoriteClasses = [];
                var bestStat = _getBestStat(attributes);
                var chosenClass = null;

                for (var i = 0; i < classes.length; i++) {
                    var aClass = classes[i];

                    if (_checkClass(aClass, attributes)) {
                        qualifiedClasses.push(aClass);
                    }
                }
                
                for (var i = 0; i < qualifiedClasses; i++) {
                    var aClass = classes[i];

                    if (aClass.favouriteAttributeId === bestStat) {
                        favoriteClasses.push(aClass);
                    }
                }
                
                if(favoriteClasses.length > 0) {
                    chosenClass = chance.pick(favoriteClasses);
                } else if (qualifiedClasses.length > 0) {
                    chosenClass = chance.pick(qualifiedClasses);
                }
                
                //Add a peasant class which is the default ?
                
                return chosenClass;
            };

            var classFactory = {
                getClass: _getClass
            };

            return classFactory;
        });


