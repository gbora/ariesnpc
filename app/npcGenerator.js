angular.module('ngNPC', ['ngChance', 'ngClassServer'])
        .factory('npcFactory', function (chance, $http, classFactory) {

            var npcSkeleton = {
                'firstName': null,
                'lastName': null,
                'sex': null,
                'title': null,
                'race': null,
                'age': null,
                'mother': null,
                'father': null,
                'siblings': null,
                'homeTown': {
                    'name': null,
                    'size': null
                },
                'socialRank': null,
                'attributesList': null,
                'characterClass': null,
                'relationships': [],
                'eventList': []

            };

            var _getSex = function () {
                return chance.bool();
            };

            var _getAge = function (race) {
                return chance.integer({
                    min: race.minAge,
                    max: race.maxAge
                });
            };

            var _getLastName = function (race) {
                var preFix = chance.pick(race.lastNames.prefixList);
                var suFix = chance.pick(race.lastNames.suffixList);

                while (preFix === suFix) {
                    suFix = chance.pick(race.lastNames.suffixList);
                }

                return preFix + suFix;
            };

            var _getFirstName = function (race, sex) {
                if (sex) {
                    return chance.pick(race.femaleFirstNames);
                } else {
                    return chance.pick(race.maleFirstNames);
                }
            };

            var _getHomeTown = function (race) {
                var homeTownSkeleton = {
                    'name': null,
                    'size': null
                };

                homeTownSkeleton.size = chance.pick(race.townSizes);
                homeTownSkeleton.name = chance.pick(race.townNames[homeTownSkeleton.size.rank]);

                return homeTownSkeleton;
            };

            var _getMother = function (race, firstName, lastName) {
                var motherSkeleton = angular.copy(npcSkeleton);
                motherSkeleton.lastName = lastName;
                motherSkeleton.firstName = _getFirstName(race, true);
                motherSkeleton.sex = true;
                motherSkeleton.title = "Mother of " + firstName + " " + lastName;
                motherSkeleton.socialRank = _getSocialStatus(race);

                return motherSkeleton;
            };

            var _getFather = function (race, firstName, lastName) {
                var fatherSkeleton = angular.copy(npcSkeleton);
                fatherSkeleton.lastName = lastName;
                fatherSkeleton.firstName = _getFirstName(race, false);
                fatherSkeleton.sex = false;
                fatherSkeleton.title = "Father of " + firstName + " " + lastName;
                fatherSkeleton.socialRank = _getSocialStatus(race);

                return fatherSkeleton;
            };

            var _getSiblings = function (race, firstName, lastName) {
                var sibChance = race.siblingChance;
                var siblings = [];

                while (sibChance > 0) {
                    if (chance.bool({likelihood: sibChance})) {
                        var sibSkeleton = angular.copy(npcSkeleton);
                        sibSkeleton.lastName = lastName;
                        sibSkeleton.sex = _getSex();
                        sibSkeleton.firstName = _getFirstName(race, sibSkeleton.sex);
                        sibSkeleton.socialRank = _getSocialStatus(race);

                        if (sibSkeleton.sex) {
                            sibSkeleton.title = "Sister of " + firstName + " " + lastName;
                        } else {
                            sibSkeleton.title = "Brother of " + firstName + " " + lastName;
                        }

                        siblings.push(sibSkeleton);

                    }

                    sibChance = sibChance - 20;
                }

                return siblings;
            };

            var _getSocialStatus = function (race) {
                return chance.pick(race.socialRanks);
            };

            var _getAttributes = function (race) {
                var standardSpread = [16, 13, 14, 12, 12, 10];

                var attributesSkeleton = [{
                        'atrributeId': 0,
                        'attributeName': 'Strength',
                        'attributeValue': 0
                    },
                    {
                        'atrributeId': 1,
                        'attributeName': 'Constitution',
                        'attributeValue': 0
                    },
                    {
                        'atrributeId': 2,
                        'attributeName': 'Dexterity',
                        'attributeValue': 0
                    },
                    {
                        'atrributeId': 3,
                        'attributeName': 'Intellect',
                        'attributeValue': 0
                    },
                    {
                        'atrributeId': 4,
                        'attributeName': 'Wisdom',
                        'attributeValue': 0
                    },
                    {
                        'atrributeId': 5,
                        'attributeName': 'Charisma',
                        'attributeValue': 0
                    }];

                for (var i = 0; i < attributesSkeleton.length; i++) {
                    var strength = chance.pick(standardSpread);
                    standardSpread.splice(standardSpread.indexOf(strength), 1);
                    attributesSkeleton[i].attributeValue = strength;
                }

                var bonuses = race.attributeBonus;

                for (var i = 0; i < bonuses.length; i++) {
                    var bonus = bonuses[i];

                    attributesSkeleton[bonus.attributeId].attributeValue += bonus.bonusValue;
                }

                return attributesSkeleton;


                //console.log(attributesSkeleton);
            };

            var _getEvents = function (npc) {
                var eventList = [];

                var socialDifference = npc.mother.socialRank.rank - npc.socialRank.rank;

                if ((npc.father.socialRank.rank - npc.socialRank.rank) > socialDifference) {
                    socialDifference = npc.father.socialRank.rank - npc.socialRank.rank;
                }

                console.log(socialDifference);

                if (socialDifference > 0) {
                    eventList.push({'title': 'Social Downfall', 'text': 'You are regarded by society as much less praiseworthy than your parent(s). Why so?'});
                } else if (socialDifference < 0) {
                    eventList.push({'title': 'Up jumped', 'text': 'You have climbed up the ladder of society through your own merits. How did you achieve this?'});
                }
                
                if(npc.characterClass) {
                    eventList.push({'title': 'Adventurous Soul', 'text': 'You have been trained with a very specific skillset. And that skillset means you can find monster and kill them how has it come to this ?'});
                }

                return eventList;
            };

            var _getFullNpc = function (title) {
                var fullNPC = npcSkeleton;

                //Summary

                fullNPC.title = title;
                fullNPC.sex = _getSex();
                return $http.get('models/racesList.json').then(function (data) {
                    fullNPC.race = chance.pick(data.data);
                    fullNPC.age = _getAge(fullNPC.race);
                    //Last Name
                    fullNPC.lastName = _getLastName(fullNPC.race);
                    //First Name
                    fullNPC.firstName = _getFirstName(fullNPC.race, fullNPC.sex);
                    //Early life
                    fullNPC.homeTown = _getHomeTown(fullNPC.race);
                    //Mother 
                    fullNPC.mother = _getMother(fullNPC.race, fullNPC.firstName, fullNPC.lastName);
                    //Father
                    fullNPC.father = _getFather(fullNPC.race, fullNPC.firstName, fullNPC.lastName);
                    //Siblings
                    fullNPC.siblings = _getSiblings(fullNPC.race, fullNPC.firstName, fullNPC.lastName);
                    //Social status
                    fullNPC.socialRank = _getSocialStatus(fullNPC.race);
                    //Attributes
                    fullNPC.attributesList = _getAttributes(fullNPC.race);

                    return $http.get('models/classesList.json').then(function (data) {
                        fullNPC.characterClass = classFactory.getClass(fullNPC.attributesList, data.data);
                        fullNPC.eventList = _getEvents(fullNPC);

                        return fullNPC;
                    });

                });
            };

            var _getNpcSkeleton = function () {
                return npcSkeleton;
            };

            var _fillRelative = function (skeleton, marriedIn) {

                if (marriedIn) {
                    var birthName = _getLastName(skeleton.race);
                    skeleton.mother = _getMother(skeleton.race, skeleton.firstName, birthName);
                    skeleton.father = _getFather(skeleton.race, skeleton.firstName, birthName);
                    skeleton.siblings = _getSiblings(skeleton.race, skeleton.firstName, birthName);
                } else {
                    skeleton.mother = _getMother(skeleton.race, skeleton.firstName, skeleton.lastName);
                    skeleton.father = _getFather(skeleton.race, skeleton.firstName, skeleton.lastName);
                    skeleton.siblings = _getSiblings(skeleton.race, skeleton.firstName, skeleton.lastName);
                }
                skeleton.age = _getAge(skeleton.race);
                skeleton.socialRank = _getSocialStatus(skeleton.race);


                return skeleton;
            };

            var npcFactory = {
                getFullNpc: _getFullNpc,
                getNpcSkeleton: _getNpcSkeleton,
                fillRelative: _fillRelative
            };
            return npcFactory;
        });